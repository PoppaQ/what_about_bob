import random

yomamaslist = ['edacious', 'pungency', 'tamper', 'turpitude', 'irrevocable', 'bilge', 'panegyric']
secure_random = random.SystemRandom()

word = secure_random.choice(yomamaslist)
secret_word = list(word)

# set the default for number of wrong answers
wrong_answer = 0

# drawings for the different phases

phase_0 = r"""
   =======
   ||
   ||
   ||
   ||
   ||
   ||
========
"""

phase_1 = r"""
   =======
   ||    |
   ||    |
   ||    O
   ||
   ||
   ||
========
"""

phase_2 = r"""
   =======
   ||    |
   ||    |
   ||    O
   ||    |
   ||
   ||
========
"""

phase_3 = r"""
   =======
   ||    |
   ||    |
   ||    O
   ||   /|
   ||
   ||
========
"""

phase_4 = r"""
   =======
   ||    |
   ||    |
   ||    O
   ||   /|\
   ||
   ||
========
"""

phase_5 = r"""
   =======
   ||    |
   ||    |
   ||    O
   ||   /|\
   ||   /
   ||
========
"""

phase_6 = r"""
   =======
   ||    |
   ||    |
   ||    O
   ||   /|\
   ||   / \
   ||
========
"""

print(phase_0)
print("enter some letter")

while len(secret_word) > 0 and wrong_answer < 6:
    some_letter = raw_input()

    if some_letter in secret_word:
        secret_word.remove(some_letter)
        print("removed: " + some_letter)
        print(str(len(secret_word)) + " letters left")

        # print the different phases of the man based on wrong answers
        if wrong_answer == 0:
            print(phase_0)
        elif wrong_answer == 1:
            print(phase_1)
        elif wrong_answer == 2:
            print(phase_2)
        elif wrong_answer == 3:
            print(phase_3)
        elif wrong_answer == 4:
            print(phase_4)
        elif wrong_answer == 5:
            print(phase_5)
    else:
        print("nope")

        # each time there is a wrong answer increase by 1
        wrong_answer = wrong_answer + 1

        # print the different phases of the man based on wrong answer 
        if wrong_answer == 1:
            print(phase_1)
        elif wrong_answer == 2:
            print(phase_2)
        elif wrong_answer == 3:
            print(phase_3)
        elif wrong_answer == 4:
            print(phase_4)
        elif wrong_answer == 5:
            print(phase_5)
        elif wrong_answer == 6:
            print(phase_6)

# print results based on number of wrong answers
if wrong_answer == 6:
    print("Sorrrrry")
else:
    print("YAAAASS: " + word)
